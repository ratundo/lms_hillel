import datetime

from django.db import models
from faker import Faker

# Create your models here.
# ORM


class Groups(models.Model):
    group_id = models.CharField(max_length=12, null=True, blank=True)
    group_leader = models.CharField(max_length=120, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    speciality = models.CharField(max_length=50, null=True, blank=True)
    average_grade = models.PositiveSmallIntegerField(null=True, blank=True)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                group_id=faker.passport_number(),
                group_leader=faker.name_nonbinary(),
                start_date=faker.date_this_decade(),
                end_date=faker.future_date(end_date=(datetime.date.today() + datetime.timedelta(days=5 * 365))),
                speciality=faker.job(),
            )

    def __str__(self):
        return f"{self.pk} {self.group_id} {self.group_leader} {self.start_date} {self.end_date}"
