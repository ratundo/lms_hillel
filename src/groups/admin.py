from django.contrib import admin  # NOQA

# Register your models here.
from groups.models import Groups

admin.site.register([Groups])
