from django.forms import ModelForm, DateInput

from groups.models import Groups


class GroupsForm(ModelForm):
    class Meta:
        model = Groups
        fields = ["group_id", "group_leader", "speciality", "start_date", "end_date"]
        widgets = {
            "start_date": DateInput(attrs={"type": "date"}),
            "end_date": DateInput(attrs={"type": "date"}),
        }
