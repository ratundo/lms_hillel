from django.urls import path
from groups.views import GroupsUpdateView, GroupsDeleteView, GroupsGetView, GroupsCreateView

app_name = "groups"  # NOQA

urlpatterns = [
    path("", GroupsGetView.as_view(), name="get_groups"),
    path("create/", GroupsCreateView.as_view(), name="create_groups"),
    path("update/<int:pk>/", GroupsUpdateView.as_view(), name="update_groups"),
    path("delete/<int:pk>/", GroupsDeleteView.as_view(), name="delete_groups"),
]
