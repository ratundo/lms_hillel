from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from groups.forms import GroupsForm
from groups.models import Groups


class GroupsCreateView(LoginRequiredMixin, CreateView):
    template_name = "person_create.html"
    form_class = GroupsForm
    success_url = reverse_lazy("groups:get_groups")


class GroupsUpdateView(LoginRequiredMixin, UpdateView):
    model = Groups
    template_name = "person_update.html"
    form_class = GroupsForm
    success_url = reverse_lazy("groups:get_groups")


class GroupsGetView(LoginRequiredMixin, ListView):
    model = Groups
    template_name = "groups_list.html"
    context_object_name = "groups"


class GroupsDeleteView(LoginRequiredMixin, DeleteView):
    model = Groups
    template_name = "groups_delete.html"
    success_url = reverse_lazy("groups:get_groups")
