from django.urls import path
from students.views import StudentUpdateView, StudentDeleteView, StudentGetView, StudentCreateView

app_name = "students"  # NOQA

urlpatterns = [
    path("", StudentGetView.as_view(), name="get_students"),
    path("create/", StudentCreateView.as_view(), name="create_students"),
    path("update/<int:pk>/", StudentUpdateView.as_view(), name="update_students"),
    path("delete/<int:pk>/", StudentDeleteView.as_view(), name="delete_students"),
]
