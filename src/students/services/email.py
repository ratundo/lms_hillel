from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


def send_registration_email(request, user_instance: get_user_model()):
    render_to_string(
        template_name="e",
        context={
            "user": user_instance,
            "domain": get_current_site(request),
            "uid": urlsafe_base64_encode(force_bytes(user_instance.pk)),
            "token": "test",
        },
    )

    email = EmailMessage(
        subject="Activate your account", body="", to=[user_instance.email], cc=[settings.EMAIL_HOST_USER]
    )
    email.send(fail_silently=settings.EMAIL_FAIL_SILENTLY)
