from django.core.validators import FileExtensionValidator
from django.db import models
from faker import Faker
import random

from main.models import Person
from students.utils.validators import validate_file_size
from groups.models import Groups


# Create your models here.
# ORM


class Student(Person):
    grade = models.PositiveSmallIntegerField(null=True, blank=True)
    group = models.ForeignKey(Groups, on_delete=models.CASCADE, default=str(random.randint(1, 20)))
    photo = models.ImageField(upload_to="students/", null=True, blank=True)
    cv = models.FileField(
        upload_to="students_cv/",
        null=True,
        blank=True,
        validators=[FileExtensionValidator(allowed_extensions=("pdf", "doc", "docx")), validate_file_size],
    )

    class Meta:
        verbose_name = "One Student"
        verbose_name_plural = "All Students"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                group_id=str(random.randint(1, 20)),
            )
