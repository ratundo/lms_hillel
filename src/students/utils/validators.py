from django.core.exceptions import ValidationError


def first_name_validator(first_name):
    if "vova" in first_name.lower():
        raise ValidationError("Vova is not valid name, should be Volodymyr")


def validate_file_size(file):
    max_file_size = 5
    max_size = max_file_size * 1048576
    if file.size > max_size:
        raise ValidationError(f"File too large, max size is {max_file_size}")
