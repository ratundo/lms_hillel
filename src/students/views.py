from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView

from students.forms import StudentForm
from students.models import Student


class StudentCreateView(LoginRequiredMixin, CreateView):
    template_name = "person_create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    template_name = "person_update.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


class StudentGetView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students_list.html"
    context_object_name = "students"

    def __init__(self):
        print("base2:", self.__class__.__base__)


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = "students_delete.html"
    success_url = reverse_lazy("students:get_students")
