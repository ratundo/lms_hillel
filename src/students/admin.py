from django.contrib import admin  # NOQA

# Register your models here.
from students.models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    ordering = ("first_name", "last_name")
    list_display = ("first_name", "last_name", "email", "birth_date")
    list_display_links = ("first_name", "email")
    list_filter = ("group__speciality",)
    search_fields = ("first_name__istartswith", "last_name__istartswith")


# admin.site.register([Student])
