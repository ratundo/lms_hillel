def format_records(records):
    return "<br>".join(str(record) for record in records)
