from django.contrib import admin  # NOQA

# Register your models here.
from teachers.models import Teachers


@admin.register(Teachers)
class TeachersAdmim(admin.ModelAdmin):
    ordering = ("first_name", "last_name")
    list_display = ("first_name", "last_name", "email", "birth_date", "groups_count")
    list_display_links = ("first_name", "email")
    list_filter = ("groups",)
    search_fields = ("first_name__istartswith", "last_name__istartswith")
    fieldsets = (
        ("Personal information", {"fields": ("first_name", "last_name", "email")}),
        # ('Additional information', {'fields':('')})
    )

    def groups_count(self, instance):
        if instance.groups:
            return instance.groups.count()
        return None
