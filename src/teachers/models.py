from django.db import models
from faker import Faker
from groups.models import Groups
from main.models import Person


# Create your models here.
# ORM


class Teachers(Person):
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    groups = models.ManyToManyField(Groups, related_name="teachers")
    photo = models.ImageField(upload_to="stuff/", null=True, blank=True)

    @classmethod  # NOQA
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            teacher = cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_of_birth(minimum_age=30, maximum_age=78),
                start_date=faker.date_this_century(),
            )
            iterations = 3
            while iterations != 0:
                random_group = Groups.objects.order_by("?").first()  # NOQA
                teacher.groups.add(random_group)
                iterations -= 1
