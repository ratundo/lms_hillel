from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView

from teachers.forms import TeacherForm
from teachers.models import Teachers


class TeachersCreateView(LoginRequiredMixin, CreateView):
    template_name = "person_create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class TeachersUpdateView(LoginRequiredMixin, UpdateView):
    model = Teachers
    template_name = "person_update.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class TeachersGetView(LoginRequiredMixin, ListView):
    model = Teachers
    template_name = "teachers_list.html"
    context_object_name = "teachers"


class TeachersDeleteView(LoginRequiredMixin, DeleteView):
    model = Teachers
    template_name = "teachers_list.html"
    success_url = reverse_lazy("teachers:get_teachers")


class TeacherGroupsView(LoginRequiredMixin, DetailView):
    model = Teachers
    template_name = "teachers_groups.html"
    context_object_name = "teacher"
    pk_url_kwarg = "pk"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        teacher = self.object
        context["teacher_groups"] = teacher.groups.all()
        return context
