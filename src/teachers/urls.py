from django.urls import path
from teachers.views import (
    TeachersUpdateView,
    TeachersDeleteView,
    TeachersGetView,
    TeachersCreateView,
    TeacherGroupsView,
)

app_name = "teachers"  # NOQA

urlpatterns = [
    path("", TeachersGetView.as_view(), name="get_teachers"),
    path("create/", TeachersCreateView.as_view(), name="create_teachers"),
    path("update/<int:pk>/", TeachersUpdateView.as_view(), name="update_teachers"),
    path("delete/<int:pk>/", TeachersDeleteView.as_view(), name="delete_teachers"),
    path("groups/<int:pk>/", TeacherGroupsView.as_view(), name="groups_of_teacher"),
]
