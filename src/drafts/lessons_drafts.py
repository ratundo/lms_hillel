from django.contrib.auth import get_user_model, login
from django.contrib.auth.forms import UserCreationForm

# from django.contrib.redirects.models import Redirect
# from django.core.mail import send_mail
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from students.services.email import send_registration_email


def index(request):
    return render(
        request,
        template_name="index.html",
        context={"name": "Me", "list_of_names": ["Anna", "Vitalii", "Gregory"]},
    )
    # return HttpResponse('Hello from Django!')


class UserRegistrationView(UserCreationForm):
    ...

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(
            request=self.request,
        )

        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request, params):
    print(params)
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            students = students.filter(or_filter)
            print(students.query.sql_with_params())
        else:
            students = students.filter(**{param_name: param_value})
        print(students.query.sql_with_params())

    return render(request, template_name="students_list.html", context={"students": students})


@csrf_exempt
def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm()
    form_html = f"""
    <form method="post">
    {form.as_p()}
    <input type="submit" value="Submit">
    </form>"""
    return HttpResponse(form_html)


@csrf_exempt
def update_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)
    return render(request, template_name="person_update.html", context={"form": form})


@csrf_exempt
def delete_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        if "submit_yes" in request.POST:
            student.delete()
        return redirect("students:get_students")
    return render(request, "student_delete.html", context={"student": student})


def first_name_validator(first_name):
    if "vova" in first_name.lower():
        raise ValidationError("Vova is not valid name, should be Volodymyr")
