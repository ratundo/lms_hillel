from django.contrib import admin  # NOQA

from main.models import Customer

# Register your models here.
admin.site.register([Customer])
