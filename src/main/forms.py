from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from config.settings import USER_TYPE_CHOICES
from main.models import UserProfile


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ["phone_number", "password1", "password2", "first_name", "last_name"]


class UserProfileForm(ModelForm):
    user_type = forms.ChoiceField(choices=USER_TYPE_CHOICES, required=False)

    class Meta:
        model = UserProfile
        fields = ["user", "patronym", "birth_day", "location", "ip_address", "user_type"]
