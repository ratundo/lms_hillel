from django.urls import path

from main.views import (
    IndexView,
    UserRegistrationView,
    UserLoginView,
    UserLogoutView,
    UserActivateView,
    send_test_email,
    UserProfileUpdateView,
)


urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("register/", UserRegistrationView.as_view(), name="register"),
    path("login", UserLoginView.as_view(), name="login"),
    path("logout", UserLogoutView.as_view(), name="logout"),
    path("activate/<str:uuid64>/<str:token>/", UserActivateView.as_view(), name="activate_user"),
    path("send_test_email", send_test_email, name="send_test_email"),
    path("profile/<int:pk>/", UserProfileUpdateView.as_view(), name="userprofile_update"),
]
