from django.conf import settings
from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_str
from django.views.generic import TemplateView, CreateView, UpdateView, RedirectView

from main.forms import UserRegistrationForm, UserProfileForm
from main.models import UserProfile
from main.services.emails import send_registration_email
from main.utils.token_generator import TokenGenerator


# Create your views here.


def handler404(request, exception):
    return render(request, "404.html")


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]
    extra_context = {"site_name": "Super puper LMS", "description": "Sator arepo tenet opera rotas"}


class UserRegistrationView(CreateView):
    template_name = "create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)

        return super().form_valid(form)


class UserActivateView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user)
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


class UserProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = UserProfile
    template_name = "person_update.html"
    form_class = UserProfileForm
    success_url = reverse_lazy("index")

    def get_object(self, queryset=None):
        pk = self.kwargs["pk"]
        return get_object_or_404(UserProfile, pk=pk)


def send_test_email(request):
    send_mail(
        subject="Test Email HW 14 Proskurin",
        message="Serge Proskurin, 26.07.2023, 17:27",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_USER, "sgop636@gmail.com", "mikolaz2727@gmail.com"],
    )
    return HttpResponse("Done")


class UserLoginView(LoginView):
    next_page = reverse_lazy("index")


class UserLogoutView(LogoutView):
    next_page = reverse_lazy("index")
