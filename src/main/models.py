from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from config.settings import USER_TYPE_CHOICES

from phonenumber_field.modelfields import PhoneNumberField

from main.managers import CustomerManager


class Customer(AbstractBaseUser, PermissionsMixin):
    """
    Phone number and password are required. Other fields are optional.
    """

    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), blank=True)
    phone_number = PhoneNumberField(_("phone number"), max_length=16, unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = CustomerManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")
        # abstract = True

    def __str__(self):
        return str(self.phone_number)

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name


class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    patronym = models.CharField(max_length=100, blank=True, null=True)
    birth_day = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to="static/img/profiles")
    location = models.CharField(max_length=300, blank=True, null=True)
    ip_address = models.GenericIPAddressField(null=True, blank=True)
    user_type = models.CharField(max_length=30, choices=USER_TYPE_CHOICES, blank=True, null=True)


# Create your models here.
class Person(models.Model):
    first_name = models.CharField(
        max_length=120, null=True, blank=True, default="no name", validators=[MinLengthValidator(2)]
    )
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True
